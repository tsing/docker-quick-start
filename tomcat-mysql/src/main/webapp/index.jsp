<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/TestDB">
select User from user
</sql:query>

<html>
<body>
<h2>Users</h2>
<c:forEach var="row" items="${rs.rows}">
  ${row.User}<br/>
</c:forEach>
</body>
</html>
